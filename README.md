# Talent Katapult 2018 GraphQL på frontenden

I dette projekt finder i koden til en enkel "one page" app hvori der skal implementeres en bookstore til at vise bøger.

## Allerede implementeret
1. Alle bøger vises ved opstart
2. Der kan søges på en Author
3. De fleste views skulle gerne allerede være implementeret, og mangler bare at blive sat sammen med data.

## Opgaver

1. Du skal udvide Books til at vise hvilke stores de er tilgængelige i.
2. Store hentes ind og der vises hvilke bøger som er til salg i den givne store
3. Hvis der klikkes på en bog, skal en data side hentes hvori alt data der er relevant for den bog vises (Store, Publisher, Author)
    - Dette må kun være i en query til serveren