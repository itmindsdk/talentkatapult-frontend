import { request } from 'graphql-request';
import { Book } from '../types';

export const GLOBAL_URL = 'http://localhost:8080/';

export class NetworkClient {
    static GetAllBooks(): Promise<{ books: Book[] }> {
        const query = `{books{name isbn}}`;
        return request<{ books: Book[] }>(GLOBAL_URL, query);
    }
}
