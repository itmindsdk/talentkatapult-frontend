import './App.css';

import * as React from 'react';

import { NetworkClient } from '../../GraphQL/NetworkClient';
import { Author, Book } from '../../types';
import Books from '../CommonComponents/Books';
import { SeachAndResultView } from './SearchAndResultView';

interface State {
    searchString: string;
    author: Author;
    books?: Book[];
}

class App extends React.Component<{}, State> {
    constructor(props: {}) {
        super(props);
        this.state = {
            searchString: '',
            author: {
                books: undefined,
                name: undefined,
            },
        };
        this.handleSearchChange = this.handleSearchChange.bind(this);
        this.onSearchPressed = this.onSearchPressed.bind(this);
    }

    async componentDidMount() {
        const returnData: { books: Book[] } = await NetworkClient.GetAllBooks();
        this.setState({books: returnData.books});
    }

    handleSearchChange(event: React.ChangeEvent<HTMLInputElement>) {
        const searchString = event.target.value;
        this.setState({searchString});
    }

    async onSearchPressed() {
       // Get authors via graphQL
        this.setState({ author: /*Set the response*/ {} });
    }

    render() {
        return (
            <div className="app">
                <div className="display-1">GraphQL CONSUMER</div>
                <div className={'main-container'}>
                    <SeachAndResultView
                        author={this.state.author}
                        handleSearchChange={this.handleSearchChange}
                        onSearchPressed={this.onSearchPressed}
                    />
                    <div className={'books-container'}/>
                    <h2 style={{marginTop: 30}}>All books from the server</h2>
                    <div className="all-books-list">
                        <Books books={this.state.books}/>
                    </div>
                </div>
            </div>
        );
    }
}

export default App;
