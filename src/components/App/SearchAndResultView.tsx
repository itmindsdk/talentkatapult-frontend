import * as React from 'react';

import { Author } from '../../types';
import SearchComponent from '../CommonComponents/SearchComponents';
import { SearchResult } from '../CommonComponents/SearchResults';

export interface SearchAndResultViewProps {
    author: Author;
    handleSearchChange(event: React.ChangeEvent<HTMLInputElement>): void;
    onSearchPressed(): void;
}

export const SeachAndResultView = (props: SearchAndResultViewProps) => (
    <div className="row">
        <div className="col">
            <SearchComponent setSearchString={props.handleSearchChange} clickSearch={props.onSearchPressed} />
        </div>
        <div className="col">
            <SearchResult {...props.author} />
        </div>
    </div>
);
