import * as React from 'react';
import { Book } from '../../types';
import { Link } from 'react-router-dom';

export function RenderBooks(books: Book[] | undefined): JSX.Element[] | null {
    if (!books) {
        return null;
    }
    return books.map((book: Book) => <Book key={book.name} {...book} />);
}

export interface BooksProps {
    books: Book[] | undefined;
}

const Book = (props: Book) => {
    return (
        <Link to={`/details/${props._id}`}>
            <div className="authorBorder">
                <div>Name: {props.name} </div>
                <br/>
                <div>ISBN: {props.isbn} </div>
            </div>
        </Link>
    );
};

class Books extends React.Component<BooksProps> {
    render() {
        return RenderBooks(this.props.books);
    }
}

export default Books;
