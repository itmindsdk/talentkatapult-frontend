import * as React from 'react';

interface SearchComponentProps {
    setSearchString(event: React.ChangeEvent<HTMLInputElement>): void;
    clickSearch(): void;
}

class SearchComponent extends React.Component<SearchComponentProps> {
    constructor(props: SearchComponentProps) {
        super(props);
    }
    render() {
        return (
            <div className={'inputStyle'}>
                <input
                    placeholder="Enter a book to search for its author"
                    type="text"
                    className={'form-control'}
                    style={{minWidth: 300, marginRight: 10}}
                    onChange={this.props.setSearchString}
                />
                <button className="btn btn-primary" onClick={this.props.clickSearch}>Search</button>
            </div>
        );
    }
}

export default SearchComponent;
