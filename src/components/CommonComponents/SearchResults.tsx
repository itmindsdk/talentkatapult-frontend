import * as React from 'react';
import { Author } from '../../types';
import './commonComponents.css';
import Books from './Books';

const NoAuthor = () => <p> no author searched yet</p>;

const Author = (props: Author) => (
    <div className="authorDiv" style={{ flex: 1, flexDirection: 'column' }}>
        <p className="h3">Author Name: {props.name} </p>
        <Books books={props.books} />
    </div>
);

export const SearchResult = (props: Author) => {
    const AuthorBooks = props.books ? Author : NoAuthor;

    return (
        <div className="search-result-container">
            <p> Search Results</p>
            <AuthorBooks {...props} />
        </div>
    );
};
