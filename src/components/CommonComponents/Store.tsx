import * as React from 'react';
import { Store as IStore } from '../../types';

export interface StoreProps {
    store: IStore;
}

export class Store extends React.Component<StoreProps> {
    render() {
        return (
            <div className="store-item">
                Name: {this.props.store.name} <br/>
                Address: {this.props.store.address}
            </div>
        );
    }
}

export const Stores = (stores: IStore[]): JSX.Element[] => {
    return stores.map((store: IStore) => {
        return (
            <Store key={store.name} store={store}/>
        );
    });
};