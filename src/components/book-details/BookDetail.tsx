import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { NetworkClient } from '../../GraphQL/NetworkClient';
import { Store } from '../../types';
import { Stores } from '../CommonComponents/Store';

interface BookDetailParams {
    bookId: string;
}

interface BookDetailProps extends RouteComponentProps<BookDetailParams> {
    name: string;
}

interface State {
    stores: Store[];
}

class BookDetail extends React.Component<BookDetailProps, State> {

    state = {
        stores: [],
    };

    async componentDidMount() {
        const re = await NetworkClient.GetStoresWithBookId(this.props.match.params.bookId);
        this.setState({stores: re.stores});
    }

    render() {
        return (
            <>
                <h4>Available in stores: </h4>
                {Stores(this.state.stores)}

            </>
        );
    }
}

export default BookDetail;