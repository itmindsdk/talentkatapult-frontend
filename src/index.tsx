import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import './index.css';
import App from './components/App/App';
import { Route, Switch } from 'react-router';
import BookDetail from './components/book-details/BookDetail';

const RootApp = () => (
    <Router>
        <Switch>
            <Route exact={true} path="/" component={App}/>
            <Route exact={true} path="/details/:bookId" component={BookDetail}/>
        </Switch>
    </Router>
);

ReactDOM.render(<RootApp/>, document.getElementById('root') as HTMLElement);
