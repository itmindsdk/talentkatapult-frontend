export interface Book {
    isbn: string;
    name: string;
    author: Author;
    _id: string;
}

export interface Author {
    name?: string;
    books?: Book[];
}

export interface Store {
    name: string; 
    address: string;
}
